import { FC } from "react";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import Homepage from "./pages/Homepage";
import LoginPage from "./pages/LoginPage";
import RegistrationPage from "./pages/RegistrationPage";
import AppLayout from "./layouts/AppLayout";
import WelcomePage from "./pages/WelcomePage";
import ProductsPage from "./pages/ProductsPage";

const router = createBrowserRouter([
  {
    path: "/",
    Component: Homepage,
  },
  {
    path: "/login",
    Component: LoginPage,
  },
  {
    path: "/registration",
    Component: RegistrationPage,
  },
  {
    path: "/",
    Component: AppLayout,
    children: [
      {
        path: "welcome",
        Component: WelcomePage,
      },
      {
        path: "products",
        Component: ProductsPage,
      },
    ],
  },
]);

const App: FC = () => {
  return <RouterProvider router={router} />;
};

export default App;
