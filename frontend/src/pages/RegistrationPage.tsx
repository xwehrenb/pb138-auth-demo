import { FC } from "react";
import { useAuthRegistration } from "../hooks/useAuth";
import { Registration } from "../models/authModels";
import RegistrationForm from "../components/RegistrationForm";
import { useNavigate } from "react-router-dom";

const RegistrationPage: FC = () => {
  const { mutateAsync: register } = useAuthRegistration();
  const navigate = useNavigate();

  const handleSubmit = async (values: Registration) => {
    try {
      await register(values);
      navigate("/login");
    } catch (e) {}
  };

  return (
    <div className="hero min-h-screen content-start pt-10 bg-base-300">
      <div className="hero-content flex-col w-full max-w-md">
        <div className="text-center">
          <h1 className="text-4xl font-bold">Register now!</h1>
        </div>
        <div className="card shrink-0 w-full bg-base-100">
          <RegistrationForm onSubmit={handleSubmit} />
        </div>
      </div>
    </div>
  );
};

export default RegistrationPage;
