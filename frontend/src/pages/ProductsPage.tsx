import { FC } from "react";
import { useProducts } from "../hooks/useProducts";

const ProductsPage: FC = () => {
  const { data: producsResp } = useProducts();

  return (
    <div className="prose">
      <h1>Products</h1>
      <table className="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {producsResp?.data.items.map((item) => (
            <tr>
              <th>{item.id}</th>
              <td>{item.name}</td>
              <td>{item.description}</td>
              <td>{item.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ProductsPage;
