import { FC } from "react";
import { useAuthLogin } from "../hooks/useAuth";
import { Login } from "../models/authModels";
import LoginForm from "../components/LoginForm";
import { useNavigate } from "react-router-dom";

const LoginPage: FC = () => {
  const { mutateAsync: login } = useAuthLogin();
  const navigate = useNavigate();

  const handleSubmit = async (values: Login) => {
    try {
      await login(values);
      navigate("/welcome");
    } catch (e) {}
  };

  return (
    <div className="hero min-h-screen content-start pt-10 bg-base-300">
      <div className="hero-content flex-col w-full max-w-md lg:flex-row-reverse">
        <div className="text-center lg:text-left">
          <h1 className="text-4xl font-bold">Login now!</h1>
        </div>
        <div className="card shrink-0 w-full bg-base-100">
          <LoginForm onSubmit={handleSubmit} />
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
