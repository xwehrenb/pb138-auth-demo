import { FC } from "react";
import PublicNavbar from "../components/PublicNavbar";
import { useProducts } from "../hooks/useProducts";

const HomePage: FC = () => {
  return (
    <div className="grid h-[100dvh] grid-rows-[min-content_1fr] bg-base-300">
      <PublicNavbar />
      <main className="px-4 py-5">
        <div className="prose">
          <h1>Homepage</h1>
          <p>
            Lorem ipsum dolor sit amet, officia excepteur ex fugiat
            reprehenderit enim labore culpa sint ad nisi Lorem pariatur mollit
            ex esse exercitation amet. Nisi anim cupidatat excepteur officia.
            Reprehenderit nostrud nostrud ipsum Lorem est aliquip amet voluptate
            voluptate dolor minim nulla est proident. Nostrud officia pariatur
            ut officia. Sit irure elit esse ea nulla sunt ex occaecat
            reprehenderit commodo officia dolor
          </p>
        </div>
      </main>
    </div>
  );
};

export default HomePage;
