import { FC } from "react";
import { Outlet } from "react-router-dom";
import AppNavbar from "../components/AppNavbar";

const AppLayout: FC = () => {
  /* TODO: Create logic, that will protect all pages in this layout */

  return (
    <div className="bg-base-300 grid h-[100dvh] grid-rows-[min-content_1fr]">
      <AppNavbar />
      <main className="px-4 py-5">
        <Outlet />
      </main>
    </div>
  );
};

export default AppLayout;
