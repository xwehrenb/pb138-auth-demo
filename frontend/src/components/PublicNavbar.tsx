import { FC } from "react";
import { NavLink } from "react-router-dom";

const PublicNavbar: FC = () => {
  return (
    <div className="navbar bg-base-100">
      <div className="flex-1">
        <NavLink to="/" className="btn btn-ghost text-xl">
          eShop
        </NavLink>
      </div>
      <div className="flex-none">
        <ul className="menu menu-horizontal px-1">
          <li>
            <NavLink to="/login">Login</NavLink>
          </li>
          <li>
            <NavLink to="/registration">Register</NavLink>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default PublicNavbar;
