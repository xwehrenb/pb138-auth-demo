import { FC } from "react";
import { Registration } from "../models/authModels";
import { SubmitHandler, useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

type RegistrationFormProps = {
  onSubmit: (values: Registration) => Promise<void>;
};

const registrationSchema = z.object({
  name: z.string(),
  email: z.string().email(),
  password: z.string(),
});

const RegistrationForm: FC<RegistrationFormProps> = (props) => {
  const { onSubmit } = props;

  const { register, handleSubmit } = useForm<Registration>({
    resolver: zodResolver(registrationSchema),
  });

  const submitHandler: SubmitHandler<Registration> = async (values) => {
    await onSubmit(values);
  };

  return (
    <form className="card-body" onSubmit={handleSubmit(submitHandler)}>
      <div className="form-control">
        <label className="label">
          <span className="label-text">Name</span>
        </label>
        <input
          {...register("name")}
          className="input input-bordered"
          required
        />
      </div>
      <div className="form-control">
        <label className="label">
          <span className="label-text">Email</span>
        </label>
        <input
          {...register("email")}
          type="email"
          className="input input-bordered"
          required
        />
      </div>
      <div className="form-control">
        <label className="label">
          <span className="label-text">Password</span>
        </label>
        <input
          {...register("password")}
          type="password"
          className="input input-bordered"
          required
        />
      </div>
      <div className="form-control mt-6">
        <button className="btn btn-primary">Register</button>
      </div>
    </form>
  );
};

export default RegistrationForm;
