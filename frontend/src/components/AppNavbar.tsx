import { FC } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { useAuthLogout, useAuthWhoAmI } from "../hooks/useAuth";

const AppNavbar: FC = () => {
  const { data: user } = useAuthWhoAmI();
  const navigate = useNavigate();

  const { mutateAsync: logout } = useAuthLogout();

  const handleLogout = async () => {
    await logout();
    navigate("/");
  };

  return (
    <div className="navbar bg-base-100">
      <div className="flex-1">
        <NavLink to="/welcome" className="btn btn-ghost text-xl">
          eShop
        </NavLink>
      </div>
      <div className="flex-none">
        <ul className="menu menu-horizontal px-1">
          <li>
            <NavLink to="/products">Products</NavLink>
          </li>
        </ul>
        <div className="dropdown dropdown-end">
          <div
            tabIndex={0}
            role="button"
            className="btn btn-neutral btn-circle avatar"
          >
            {user?.data.name
              .split(" ")
              .map((np) => np[0] ?? "")
              .join("")}
          </div>
          <ul
            tabIndex={0}
            className="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52"
          >
            <li>
              <button type="button" onClick={handleLogout}>
                Logout
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default AppNavbar;
