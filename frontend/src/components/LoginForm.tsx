import { FC } from "react";
import { Login } from "../models/loginModels";
import { SubmitHandler, useForm } from "react-hook-form";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";

type LoginFormProps = {
  onSubmit: (values: Login) => Promise<void>;
};

const loginSchema = z.object({
  email: z.string().email(),
  password: z.string(),
});

const LoginForm: FC<LoginFormProps> = (props) => {
  const { onSubmit } = props;

  const { register, handleSubmit } = useForm<Login>({
    resolver: zodResolver(loginSchema),
  });

  const submitHandler: SubmitHandler<Login> = async (values) => {
    await onSubmit(values);
  };

  return (
    <form className="card-body" onSubmit={handleSubmit(submitHandler)}>
      <div className="form-control">
        <label className="label">
          <span className="label-text">Email</span>
        </label>
        <input
          type="email"
          {...register("email")}
          className="input input-bordered"
          required
        />
      </div>
      <div className="form-control">
        <label className="label">
          <span className="label-text">Password</span>
        </label>
        <input
          type="password"
          {...register("password")}
          className="input input-bordered"
          required
        />
      </div>
      <div className="form-control mt-6">
        <button className="btn btn-primary">Login</button>
      </div>
    </form>
  );
};

export default LoginForm;
