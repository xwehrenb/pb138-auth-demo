import { useQuery } from "react-query";
import { ProductApi } from "../services/productApi";

export const useProducts = () => {
  return useQuery({
    queryKey: ["products"],
    queryFn: () => ProductApi.getAll(),
  });
};
