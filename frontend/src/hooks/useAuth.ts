import { useMutation, useQuery } from "react-query";
import { AuthApi } from "../services/authApi";
import { Login, Registration } from "../models/authModels";

export const useAuthLogin = () => {
  return useMutation({
    mutationFn: (payload: Login) => AuthApi.login(payload),
  });
};

export const useAuthRegistration = () => {
  return useMutation({
    mutationFn: (payload: Registration) => AuthApi.register(payload),
  });
};

export const useAuthLogout = () => {
  return useMutation({
    mutationFn: () => AuthApi.logout(),
  });
};

export const useAuthWhoAmI = () => {
  return useQuery({
    queryKey: ["whoami"],
    queryFn: () => AuthApi.whoami(),
    retry: 1,
  });
};
