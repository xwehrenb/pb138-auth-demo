import { Product } from "../models/productModels";
import { BaseApi } from "./baseApi";

function getAll() {
  return BaseApi.get<{ items: Product[] }>("/products");
}

export const ProductApi = {
  getAll,
};
