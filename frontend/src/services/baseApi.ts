import axios from "axios";

export const BaseApi = axios.create({
  baseURL: "http://localhost:4000",
  headers: {
    "Access-Control-Allow-Credentials": true,
  },
  withCredentials: true,
});
