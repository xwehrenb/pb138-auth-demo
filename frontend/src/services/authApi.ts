import { Login, Registration, WhoAmI } from "../models/authModels";
import { BaseApi } from "./baseApi";

function login(payload: Login) {
  return BaseApi.post("/auth/login", payload);
}

function register(payload: Registration) {
  return BaseApi.post("/auth/registration", payload);
}

function logout() {
  return BaseApi.get("/auth/logout");
}

function whoami() {
  return BaseApi.get<WhoAmI>("/auth/whoami");
}

export const AuthApi = {
  login,
  register,
  logout,
  whoami,
};
