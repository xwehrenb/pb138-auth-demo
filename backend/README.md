# Alza API 2: Security issues encountered!

Operators of Alza API from seminar 6 have noticed some products are being created without anyone knowing anything about it!
They suspect that someone is using the API to create products without proper authentication. You are asked to implement a simple authentication system to prevent this from happening.

In this assignment, you will be implementing a simple authentication system for a web application.

## Requirements

- Node.js, npm
- docker with a Redis container running on port 6379
  - `docker run -d -p 6379:6379 redis`
- Installed dependencies (`npm i`)

## (Optional) Advanced setup

In this exercise, you can use Redis as a general purpose store for both session data and user data for simplicity.

However, in a real-world scenario, you would use a database like PostgreSQL. Feel free to set it up this way and use `prisma`/`kysely`/`drizzleorm` to interact with the database instead of Redis client.

## Instructions

1. Get familiar with the codebase!

(Or don't, you probably seen it already in seminar 6)

2. In `src/auth/types.ts`, fill in the `User` type.

3. Complete the validation schemas in `src/auth/validationSchemas.ts`

4. Implement the `register` route in `src/auth/routes.ts`.

   - Do not forget to hash the password before storing it in the database!

5. Install `passport` and `passport-local` for authentication.

   - Consult the [documentation](https://www.passportjs.org/) and [tutorials](https://www.passportjs.org/tutorials/password/) section of the docs on how to use the library!

6. Configure `passport` to use `passport-local` strategy.

7. Implement the `login` route in `src/auth/routes.ts`.

8. Implement the `logout` route in `src/auth/routes.ts`.

9. Utilize `express-session` to pass the session-ID to the client via `cookies`.

   - Consult the [documentation](https://www.npmjs.com/package/express-session) on how to use the library!
   - (Optional) Use Redis as a session store.

10. **Protect all endpoints that manipulate the product catalog!**
    - Leave GET endpoints unprotected.

## Tips

- Use a client like [Insomnium](https://github.com/ArchGPT/insomnium) to test your implementation.

## AC:

- [ ] User can register with a username and password.
- [ ] User can log in with the registered username and password.
- [ ] User can log out.
- [ ] User can't modify the product catalog without being logged in.
- [ ] User can view the product catalog without being logged in.
