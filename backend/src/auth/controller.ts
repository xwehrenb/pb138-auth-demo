import { NextFunction, Request, Response } from "express";
import { userRepository } from "./repository";
import { registerSchema } from "./validationSchemas";
import { fromZodError } from "zod-validation-error";
import { handleErrors } from "../utils";

const register = async (req: Request, res: Response) => {
  const validRequest = await registerSchema.safeParseAsync(req);

  if (!validRequest.success) {
    const error = fromZodError(validRequest.error);
    const errorResponse: Error = {
      name: "ValidationError",
      message: error.message,
      cause: error.cause,
    };
    res.status(400).send(errorResponse);
    return;
  }

  const { name, email, password } = validRequest.data.body;

  const userExists = await userRepository.checkExists(email);

  if (userExists.isErr) {
    handleErrors(userExists.error, res);
    return;
  }

  if (userExists.value) {
    res.status(400).send({ message: "User already exists" });
    return;
  }

  const user = await userRepository.create({ name, email, password });

  if (user.isErr) {
    handleErrors(user.error, res);
    return;
  }

  res.status(201).end();
};

const login = async (_req: Request, res: Response) => {
  res.status(200).end();
};

const whoami = async (req: Request, res: Response) => {
  res.json(req.session.passport?.user).status(200);
};

const logout = async (req: Request, res: Response, next: NextFunction) => {
  return req.logout(
    {
      keepSessionInfo: false,
    },
    (err) => {
      if (err) return next(err);
      res.status(200).end();
    },
  );
};

export const authController = {
  register,
  login,
  logout,
  whoami,
};
