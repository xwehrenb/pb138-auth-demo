import { Router } from "express";
import { authController } from "./controller";
import passport from "../passportConfig";
import { setupAuthMiddleware } from "../middleware/authMiddleware";

export const authRouter = Router();

authRouter.post("/registration", authController.register);
authRouter.post("/login", passport.authenticate("local"), authController.login);

authRouter.use(passport.session());

authRouter.get("/logout", authController.logout);

authRouter.use(setupAuthMiddleware());

authRouter.get("/whoami", authController.whoami);
