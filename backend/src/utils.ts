import { Request, Response } from "express";
import { ZodSchema, ZodTypeDef } from "zod";
import { fromZodError } from "zod-validation-error";

export class NotFoundError extends Error {
  public name = "NotFoundError";
  public message = "Entity not found";
}

export class UnimplementedError extends Error {
  public name = "UnimplementedError";
  public message = "This feature is not implemented yet.";
}

export class InternalError extends Error {
  public name = "InternalError";
  public message = "Something went wrong on our side.";
}

export const handleErrors = (e: Error, res: Response) => {
  if (e instanceof NotFoundError) {
    const { name, message, cause } = e;
    const response: Error = { name, message, cause };
    res.status(404).send(response);
    return;
  }

  if (e instanceof UnimplementedError) {
    const { name, message, cause } = e;
    const response: Error = { name, message, cause };
    res.status(501).send(response);
    return;
  }

  const { name, message, cause } = e;
  if (!(e instanceof InternalError)) {
    e = new InternalError();
  }

  const response: Error = { name, message, cause };
  res.status(500).send(response);
};

export const parseRequest = async <
  Output,
  Def extends ZodTypeDef = ZodTypeDef,
  Input = Output
>(
  schema: ZodSchema<Output, Def, Input>,
  req: Request,
  res: Response
) => {
  const parsedRequest = await schema.safeParseAsync(req);

  if (!parsedRequest.success) {
    const error = fromZodError(parsedRequest.error);
    const errorResponse: Error = {
      name: "ValidationError",
      message: error.message,
      cause: error.cause,
    };
    res.status(400).send(errorResponse);
    return null;
  }

  return parsedRequest.data;
};
