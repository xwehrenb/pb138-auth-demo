import express from "express";
import { productsRouter } from "./products/router";
import { authRouter } from "./auth/router";

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/auth", authRouter);

/*Protect all product routes */
app.use("/products", productsRouter);

app.use((_req, res) => res.status(404).send("Not found"));

app.listen(process.env.API_PORT, () =>
  console.log(`Eshop API listening on port ${process.env.API_PORT}`),
);
