import RedisStore from "connect-redis";
import { redisClient } from "../redisClient";

export const setupSessionMiddleware = () => {
  const redisStore = new RedisStore({
    client: redisClient,
    prefix: "x-session:",
  });

  return; /* TODO: init session with redis storage */
};
