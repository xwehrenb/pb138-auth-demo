import { Request, Response, NextFunction } from "express";

declare module "express-session" {
  interface SessionData {
    passport: {
      user: { username: string; id: string };
    };
  }
}

export const setupAuthMiddleware = () => {
  return; /* setup the authentication middleware */
};
