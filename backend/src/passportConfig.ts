import { Passport } from "passport";
import { User } from "./auth/types";

const passport = new Passport();

export const passportStrategy = () => {
  return; /* create auth local passport strategy */
};

passport.use(passportStrategy());

passport.serializeUser((_user, cb) => {
  process.nextTick(() => {
    const user = _user as User;
    return cb(null, {
      id: user.id,
      name: user.name,
    });
  });
});

passport.deserializeUser((user, cb) => {
  process.nextTick(() => {
    return cb(null, user!);
  });
});

export default passport;
