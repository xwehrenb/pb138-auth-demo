import { Router } from "express";
import { productsController } from "./controller";

export const productsRouter = Router();

productsRouter.get("/", productsController.getProducts);
