import { Request, Response } from "express";
import { Product } from "./types";
import { faker } from "@faker-js/faker";

const mockedProducts: Product[] = new Array(10).fill(null).map(() => ({
  id: faker.number.int({ min: 100, max: 999 }),
  name: faker.commerce.productName(),
  description: faker.commerce.productDescription(),
  price: faker.number.int({ min: 20, max: 400 }),
}));

const getProducts = async (_req: Request, res: Response) => {
  res.json({ items: mockedProducts });
};

export const productsController = {
  getProducts,
};
